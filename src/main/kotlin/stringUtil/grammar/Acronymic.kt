package stringUtil.grammar

import stringUtil.ACRONYMIC_CONSONANTS
import stringUtil.ACRONYMIC_VOWELS

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Checks if the given [String] starts with an Acronymic Vowel
 * @see [ACRONYMIC_VOWELS]
 * @return [Boolean]
 */
fun String.startsWithAcroVowel(): Boolean =
    if (this.isNotEmpty()) {
        this[0] in ACRONYMIC_VOWELS
    } else {
        false
    }

/**
 * Checks if the given [String] starts with an Acronymic Consonant
 * @see [ACRONYMIC_CONSONANTS]
 * @return [Boolean]
 */
fun String.startsWithAcroConsonant(): Boolean =
    if (this.isNotEmpty()) {
        this[0] in ACRONYMIC_CONSONANTS
    } else {
        false
    }