package stringUtil.grammar

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Checks if the given [String] starts with a vowel
 *
 * @param[alphabetic] Whether or not to check if the [String] starts with an alphabetic vowel
 * @return [Boolean]
 */
@JvmOverloads fun String.startsWithVowel(alphabetic: Boolean = true): Boolean =
    if (alphabetic) {
        this.startsWithAlphaVowel()
    } else {
        this.startsWithAcroVowel()
    }

/**
 * Checks if the given [String] starts with a consonant
 *
 * @param[alphabetic] Whether or not to check if the [String] starts with an alphabetic consonant
 * @return [Boolean]
 */
@JvmOverloads fun String.startsWithConsonant(alphabetic: Boolean=true): Boolean =
    if (alphabetic) {
        this.startsWithAlphaConsonant()
    } else {
        this.startsWithAcroConsonant()
    }

