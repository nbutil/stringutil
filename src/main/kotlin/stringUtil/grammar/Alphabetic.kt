package stringUtil.grammar

import stringUtil.ALPHABETIC_CONSONANTS
import stringUtil.ALPHABETIC_VOWELS

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Checks if the given [String] starts with an alphabetic vowel
 * @see [ALPHABETIC_VOWELS]
 * @return [Boolean]
 */
fun String.startsWithAlphaVowel(): Boolean =
    if (this.isNotEmpty()) {
        if (this[0].toLowerCase() == 'y' && this.length > 1) {
            // For words like ypsilon, yvonne, etc.
            this[1] in ALPHABETIC_CONSONANTS
        } else {
            this[0] in ALPHABETIC_VOWELS
        }
    } else {
        false
    }

/**
 * Checks if the given [String] starts with an alphabetic consonant
 * @see [ALPHABETIC_CONSONANTS]
 * @return [Boolean]
 */
fun String.startsWithAlphaConsonant(): Boolean =
    if (this.isNotEmpty()) {
        if (this[0].toLowerCase() == 'y' && this.length > 1) {
            this[1] in ALPHABETIC_VOWELS
        } else {
            this[0] in ALPHABETIC_CONSONANTS
        }
    } else {
        false
    }

