package stringUtil.grammar

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Gets the corrent indefinite article for the given [String].
 * This function assumes the given string is singular.
 *
 * @return [String]
 */
fun String.getIndefiniteArticle(): String =
    if (this.all { it.isUpperCase() }) {
        if (this.startsWithAcroVowel()) {
            "an"
        } else {
            "a"
        }
    } else {
        if (this.startsWithAlphaVowel()) {
            "an"
        } else {
            "a"
        }
    }