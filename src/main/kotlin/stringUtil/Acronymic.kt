package stringUtil

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Acronymic refers to what you would say when saying the name of the character.
 * E.g.: "An A" or "A V", not "A 8" or "An C"
 */

val ACRONYMIC_VOWELS = charArrayOf(
    'a', 'e', 'f', 'h', 'i', 'l', 'm', 'n', 'o', 'r', 's', 'x',
    'A', 'E', 'F', 'H', 'I', 'L', 'M', 'N', 'O', 'R', 'S', 'X', '8'
)

val ACRONYMIC_CONSONANTS = charArrayOf(
    'b', 'c', 'd', 'g', 'j', 'k', 'p', 'q', 't', 'u', 'v', 'w', 'y', 'z',
    'B', 'C', 'D', 'G', 'J', 'K', 'P', 'Q', 'T', 'U', 'V', 'W', 'Y', 'Z',
    '0', '1', '2', '3', '4', '5', '6', '7', '9'
)