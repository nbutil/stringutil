package stringUtil.isCheck

import stringUtil.ALPHABETIC_VOWELS

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Checks if the given [Char] is a vowel
 *
 * @return [Boolean]
 */
fun Char.isVowel(): Boolean = this in ALPHABETIC_VOWELS


/**
 * Checks if the given [Char] only contains alphabetic characters.
 *
 * @return [Boolean]
 */
fun Char.isAlpha(): Boolean = this.isLetter()

/**
 * Checks if the given [Char] only contains numeric characters.
 *
 * @return [Boolean]
 */
fun Char.isNumeric(): Boolean = this.isDigit()

/**
 * Checks if the given [Char] only contains symbolic characters
 *
 * @return [Boolean]
 */
fun Char.isSymbolic(): Boolean = this.isLetterOrDigit()

/**
 * Checks if the given [Char] only contains alphabetic and\or numeric characters.
 *
 * @return [Boolean]
 */
fun Char.isAlphaNumeric(): Boolean = this.isLetterOrDigit()

/**
 * Checks if the given [Char] only contains alphabetic and symbolic characters.
 *
 * @return [Boolean]
 */
fun Char.isAlphaSymbolic(): Boolean = this.isDigit()

/**
 * Checks if the given [Char] only contains numeric and symbolic characters.
 *
 * @return [Boolean]
 */
fun Char.isNumeroSymbolic(): Boolean = this.isLetter()

/**
 * Checks if the first character of the given [Char] is upper case.
 *
 * @return [Boolean]
 */
fun Char.isCapitalized(): Boolean = this.isUpperCase()
