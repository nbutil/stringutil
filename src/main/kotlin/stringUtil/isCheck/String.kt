package stringUtil.isCheck

import stringUtil.ALPHABETIC_VOWELS

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Checks if the given [String] is a vowel
 *
 * @return [Boolean]
 */
fun String.isVowel(): Boolean =
    if (this.length == 1) {
        this[0] in ALPHABETIC_VOWELS
    } else {
        false
    }

/**
 * Checks if the given [String] only contains alphabetic characters.
 *
 * @return [Boolean]
 */
fun String.isAlpha(): Boolean = this.all { it.isLetter() }

/**
 * Checks if the given [String] only contains numeric characters.
 *
 * @return [Boolean]
 */
fun String.isNumeric(): Boolean = this.all { it.isDigit() }

/**
 * Checks if the given [String] only contains symbolic characters
 *
 * @return [Boolean]
 */
fun String.isSymbolic(): Boolean = this.all { !it.isLetterOrDigit() }

/**
 * Checks if the given [String] only contains alphabetic and\or numeric characters.
 *
 * @return [Boolean]
 */
fun String.isAlphaNumeric(): Boolean = this.all { it.isLetterOrDigit() }

/**
 * Checks if the given [String] only contains alphabetic and symbolic characters.
 *
 * @return [Boolean]
 */
fun String.isAlphaSymbolic(): Boolean = this.all { it.isDigit() }

/**
 * Checks if the given [String] only contains numeric and symbolic characters.
 *
 * @return [Boolean]
 */
fun String.isNumeroSymbolic(): Boolean = this.all { it.isLetter() }

/**
 * Checks if the first character of the given [String] is upper case.
 *
 * @return [Boolean]
 */
fun String.isCapitalized(): Boolean = this[0].isUpperCase()

/**
 * Checks if the given [String] is all lower case.
 *
 * @return [Boolean]
 */
fun String.isUpperCase(): Boolean = this.all { it.isUpperCase() }

/**
 * Checks if the given [String] is all lower case.
 *
 * @return [Boolean]
 */
fun String.isLowerCase(): Boolean = this.all { it.isLowerCase() }
