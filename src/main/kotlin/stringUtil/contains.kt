package stringUtil

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Checks if [other] isCheck contained, ignoring case.
 *
 * @param[other] the [String] to check isCheck contained
 *
 * @return [Boolean]
 */
fun String.containsIgnoreCase(other: String) = other.toLowerCase() in this.toLowerCase()


/**
 * Checks if [other] isCheck contained, ignoring case.
 *
 * @param[other] The [String] to check isCheck contained
 *
 * @return [Boolean]
 */
fun Iterable<String>.containsIgnoreCase(other: String) = any { it.equals(other, ignoreCase=true) }

