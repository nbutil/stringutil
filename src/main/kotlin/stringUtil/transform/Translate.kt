package stringUtil.transform

/** @author: Curtis Woodard <nbness1337@gmail.com> */


/**
 * Exception for Translation table size mismatch
 */
class TranslationTableSizeMismatch(override val message: String?) : Exception()

/**
 * Allows you to easily do a simple string translation between [inChars] and [outChars]
 *
 * @property [inChars] The characters the table receives that correspond to the characters in [outChars]
 * @property [outChars] The characters the table emits that correspond to the characters in [inChars]
 */
class TranslationTable(inChars: CharArray, outChars: CharArray) {
    /**
     * @constructor Creates a [TranslationTable] from 2 [String] rather than 2 [CharArray]
     */
    constructor(inChars: String, outChars: String):
            this(inChars.toCharArray(), outChars.toCharArray())

    private val inChars: CharArray
    private val outChars: CharArray
    init {
        if (inChars.size != outChars.size) {
            throw TranslationTableSizeMismatch("inChars and outChars must be the same length to create a TranslationTable")
        }
        this.inChars = inChars
        this.outChars = outChars
    }

    /**
     * Gets the output character that corresponds to [inChar].
     * Returns [inChar] if there is no corresponding output character.
     *
     * @param[inChar] The character you are getting the output character for
     *
     * @return [Char]
     */
    fun getCharFor(inChar: Char): Char {
        if (inChar in inChars) {
            return outChars[inChars.indexOf(inChar)]
        }
        return inChar
    }

    /**
     * Gets the input character that corresponds to [outChar].
     * Returns [outChar] if there is no corresponding input character.
     *
     * @param[outChar] The character you are getting the input character for
     *
     * @return [Char]
     */
    fun reverseGetCharFor(outChar: Char): Char {
        if (outChar in outChars) {
            return inChars[outChars.indexOf(outChar)]
        }
        return outChar
    }

    /**
     * Translates [inString] from [inChars] to [outChars]
     *
     * @param[inString] The [String] to translate
     *
     * @return [String]
     */
    fun translateString(inString: String): String =
        inString
            .map { getCharFor(it) }
            .joinToString("", "", "")

    /**
     * Translates [inString] from [outChars] to [inChars]
     *
     * @param[inString] The [String] to translate
     *
     * @return [String]
     */
    fun reverseTranslateString(inString: String): String =
        inString
            .map{ reverseGetCharFor(it) }
            .joinToString("", "", "")

    /**
     * Gives a reversed versino of this [TranslationTable]
     *
     * @return [TranslationTable]
     */
    fun reversedTranslationTable(): TranslationTable = TranslationTable(outChars, inChars)
}

/** @see [TranslationTable.translateString]*/
fun String.translate(table: TranslationTable): String = table.translateString(this)
/** @see [TranslationTable.reverseTranslateString] */
fun String.reverseTranslate(table: TranslationTable): String = table.reverseTranslateString(this)
