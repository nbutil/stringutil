package stringUtil.transform

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Returns a [String] with all exact instances of [toRemove] removed
 *
 * @param [toRemove] The character to remove
 *
 * @return [String]
 */
fun String.remove(toRemove: Char): String {
    var finalString = ""
    for (character in this) {
        if (character != toRemove) {
            finalString += character
        }
    }
    return finalString
}

fun String.remove(toRemove: String): String {
    if (toRemove.isEmpty()) return this
    if (toRemove.length == 1) return remove(this[0])
    var finalString = ""
    var slicedString = ""
    var slicedIndex = 0
    var sliceMax = 0
    for ((index, character) in this.withIndex()) {
        sliceMax = index + toRemove.length
        if (sliceMax >= this.length) {
            sliceMax = this.length
        }
        slicedString = this.slice(index until sliceMax)
        if (slicedString == toRemove) {
            slicedIndex = toRemove.length
        }
        if (slicedIndex > 0) {
            slicedIndex --
        } else {
            finalString += character
        }
    }
    return finalString
}
