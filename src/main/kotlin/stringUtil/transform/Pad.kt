package stringUtil.transform

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Pads the left side of the string with [padChar] until it is [toLength] long
 *
 * @param[toLength] The length to pad the string to
 * @param[padChar] The character to pad the string with
 * @return[String]
 */
fun String.padLeft(toLength: Int, padChar: String): String {
    if (this.length >= toLength) {
        return this
    }
    if (padChar.length != 1)
        throw IndexOutOfBoundsException("padCher must be 1 character long, not ${padChar.length}.")
    var returnString = ""
    for (index in this.length until toLength) {
        returnString += padChar
    }
    return returnString + this
}

fun String.padLeft(toLength: Int, padChar: Char): String {
    if (this.length >= toLength) {
        return this
    }

    var returnString = ""
    for (index in this.length until toLength) {
        returnString += padChar
    }
    return returnString + this
}


/**
 * Pads the right side of the string with [padChar] until it is [toLength] long
 *
 * @param[toLength] The length to pad the string to
 * @param[padChar] The character to pad the string with
 * @return[String]
 */
fun String.padRight(toLength: Int, padChar: String): String {
    if (this.length >= toLength) {
        return this
    }
    if (padChar.length != 1)
        throw IndexOutOfBoundsException("padCher must be 1 character long, not ${padChar.length}.")
    var returnString = this
    for (index in this.length until toLength) {
        returnString += padChar
    }
    return returnString
}

fun String.padRight(toLength: Int, padChar: Char): String {
    if (this.length >= toLength) {
        return this
    }

    var returnString = this
    for (index in this.length until toLength) {
        returnString += padChar
    }
    return returnString
}
