package stringUtil

/** @author: Curtis Woodard <nbness1337@gmail.com> */

/**
 * Alphabetic refers to what you would say if the word started with that letter.
 * E.g.: "An apple" or "A dog", not "An cookie" or "A igloo"
 */
val ALPHABETIC_VOWELS: List<Char> = listOf(
    'a', 'e', 'i', 'o', 'u',
    'A', 'E', 'I', 'O', 'U'
)

val ALPHABETIC_CONSONANTS: List<Char> = listOf(
    'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z',
    'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z'
)